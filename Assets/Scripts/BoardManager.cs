﻿using UnityEngine;
using System.Collections.Generic;

public class BoardManager : MonoBehaviour
{
    public int nrOfColumns = 10;
    public int nrOfRows = 10;

    public GameObject[] floorTiles;
    public GameObject[] wallTiles;
    public GameObject[] horizontalSpawnPositionTiles;
    public GameObject[] verticalSpawnPositionTiles;
    public GameObject player;
    public GameObject enemy;
    public GameObject barricade;

    private Transform boardHolder;
    private List<Vector3> spawnPositions = new List<Vector3>();

    private void InitialiseList ()
    {
        for (int x = 0; x <= nrOfColumns; x++)
        {
            for (int y = 0; y <= nrOfRows; y++)
            {
                if ((x == nrOfColumns / 2 && (y == 0 || y == nrOfRows )) || (y == nrOfRows / 2 && (x == 0 || x == nrOfColumns)))
                {
                    spawnPositions.Add(new Vector3(x, y, 0f));
                }
            }
        }
	}

    private void BoardSetup()
    {
        boardHolder = new GameObject("Board").transform;
        
        for (int x = -1; x <= nrOfColumns +1; x++)
        {
            for (int y = -1; y <= nrOfRows +1; y++)
            {
                GameObject toInstantiate = null;
                if (x == nrOfColumns / 2 && (y == 0 || y == nrOfRows))
                {
                    toInstantiate = verticalSpawnPositionTiles[Random.Range(0, verticalSpawnPositionTiles.Length)];
                }
                else if (y == nrOfRows / 2 && (x == 0 || x == nrOfColumns))
                {
                    toInstantiate = horizontalSpawnPositionTiles[Random.Range(0, horizontalSpawnPositionTiles.Length)];
                }
                else if (x <= 0 || x >= nrOfColumns || y <= 0 || y >= nrOfRows)
                {
                    toInstantiate = wallTiles[Random.Range(0, wallTiles.Length)];
                }
                else
                {
                    toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];
                }

                GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;

                instance.transform.SetParent(boardHolder);
            }
        }
    }

    public void SetupBoard()
    {
        InitialiseList();
        BoardSetup();
    }

    public void SpawnPlayer()
    {
        var spawnX = nrOfRows / 2;
        var spawnY = nrOfColumns / 2;

        GameObject instance = Instantiate(player, new Vector3(spawnX, spawnY, 0f), Quaternion.identity) as GameObject;
    }

    public void SpawnEnemy()
    {
        var spawnPosition = spawnPositions[Random.Range(0, spawnPositions.Count)];
        
        GameObject instance = Instantiate(enemy, new Vector3(spawnPosition.x, spawnPosition.y, 0f), Quaternion.identity) as GameObject;
    }

    public void SpawnBarricade(float x, float y)
    {
        GameObject instance = Instantiate(barricade, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
    }
}
