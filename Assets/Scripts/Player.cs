﻿using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int damage = 1;
    public int initialHealth = 3;
    public float movementSpeed = 1f;
    private Text healthText;
    private int health;
    private Animator animator;
    private Rigidbody2D rigidBody2d;
    private Camera mainCamera;
    private Transform playerTransform;
    private int horizontalDir = 0;
    private int verticalDir = 0;
    public float barricadeTimer = 5;
    private float timer = 3f;

    void Start()
    {
        animator = GetComponent<Animator>();
        health = initialHealth;
        rigidBody2d = GetComponent<Rigidbody2D>();
        mainCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
        playerTransform = GetComponent<Transform>();
        healthText = GameObject.Find("Life").GetComponent<Text>();
    }

    void Update()
    {
        healthText.text = string.Format("Life: {0}", health);
    }

    void FixedUpdate ()
    {
        timer += Time.deltaTime;

        int horizontal = 0;
        int vertical = 0;

        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");
                
        Move(horizontal, vertical);

        if (horizontal != 0 || vertical != 0)
        {
            horizontalDir = horizontal;
            verticalDir = vertical;
        }

        if (Input.GetKeyDown(KeyCode.Space) && timer > barricadeTimer)
        {
            timer = 0;
            SpawnBarricade();
        }

        mainCamera.transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y, mainCamera.transform.position.z);
    }

    private void SpawnBarricade()
    {
        var boardManager = (BoardManager)GameObject.FindWithTag("GameController").GetComponent(typeof(BoardManager));
        boardManager.SpawnBarricade(playerTransform.position.x + 0.22f * horizontalDir, playerTransform.position.y + 0.44f * verticalDir);
    }

    private void Move(int horizontal, int vertical)
    {
        rigidBody2d.velocity = new Vector2(horizontal * movementSpeed, vertical * movementSpeed);
    }
    
    public void LoseHealth(int damage)
    {
        health -= damage;
        if (IsGameOver())
        {
            enabled = false;
            GameManager.instance.GameOver();
        }
    }
    
    private bool IsGameOver()
    {
        return health < 1;
    }
}
