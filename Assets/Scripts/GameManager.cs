﻿using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public static GameManager instance = null;
    public float spawnTimer = 7f;
    private Text gameoverText;
    private Text respawnText;
    private BoardManager boardManager;
    private float timer = 0f;
    private bool isGameOver = false;

    // Awake is always called before any Start functions
    void Awake ()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        boardManager = GetComponent<BoardManager>();
        InitGame();
        gameoverText = GameObject.Find("GameOver").GetComponent<Text>();
        respawnText = GameObject.Find("Respawn").GetComponent<Text>();
        gameoverText.enabled = false;
        respawnText.enabled = false;
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= spawnTimer)
        {
            boardManager.SpawnEnemy();
            timer = 0f;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("Main");
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }


    }

    private void InitGame()
    {
        boardManager.SetupBoard();
        boardManager.SpawnPlayer();
    }

    public void GameOver()
    {
        if (!isGameOver)
        {
            gameoverText.text = string.Format("Game Over! You survived for {0} seconds", Math.Round(Time.timeSinceLevelLoad, 2));
            gameoverText.enabled = true;
            respawnText.enabled = true;
            isGameOver = true;
        }
    }
}
