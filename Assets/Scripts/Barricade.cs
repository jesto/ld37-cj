﻿using UnityEngine;

public class Barricade : MonoBehaviour
{
    public Sprite[] sprites;
    public int healthPerSprite = 2;

    private SpriteRenderer spriteRenderer;
    private int health;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        health = healthPerSprite * sprites.Length;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        if(health <= 0)
        {
            var boardManager = (BoardManager)GameObject.FindWithTag("GameController").GetComponent(typeof(BoardManager));
            var transform = GetComponent<Transform>();
            Destroy(gameObject);
        }

        var spriteIndex = sprites.Length - (health / healthPerSprite);
        if (spriteIndex >= sprites.Length)
            spriteIndex = sprites.Length -1;
        spriteRenderer.sprite = sprites[spriteIndex];
    }
}
