﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    public int damage = 1;
    public int initialHealth = 3;
    public float movementSpeed = 1f;

    private Rigidbody2D rigidBody2d;
    private Animator animator;
    private float attackTimer = 2f;
    private float timer = 0f;

    void Start ()
    {
        animator = GetComponent<Animator>();
        rigidBody2d = GetComponent<Rigidbody2D>();
    }

    void Update ()
    {
        timer += Time.deltaTime;

        var position = GetComponent<Transform>().position;
        var target = GameObject.FindWithTag("Player").transform.position;

        var horizontal = 0;
        if (target.x <= position.x)
            horizontal = -1;
        else
            horizontal = 1;

        var vertical = 0;
        if (target.y <= position.y)
            vertical = -1;
        else
            vertical = 1;

        Move(horizontal, vertical);
    }

    void OnTriggerStay2D(Collider2D col)
    {
        timer += Time.deltaTime;

        if (timer < attackTimer)
        {
            return;
        }

        if (col.gameObject.tag == "Player")
        {
            var player = (Player)col.gameObject.GetComponent(typeof(Player));
            player.LoseHealth(damage);
        }
        else if (col.gameObject.tag == "Barricade")
        {
            var barricade = (Barricade)col.gameObject.GetComponent(typeof(Barricade));
            barricade.TakeDamage(damage);
        }
        timer = 0f;
    }

    private void Move(int horizontal, int vertical)
    {
        rigidBody2d.velocity = new Vector2(horizontal * movementSpeed, vertical * movementSpeed);
    }
}
